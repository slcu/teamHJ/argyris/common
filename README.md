To read the L1 and return a pandas DataFrame with the position, volume:

```
import common.seg as seg

fnSeg = "seg.tif"
res = [0.2, 0.1, 0.1]
background=1
d = seg.readGeomImL1Pandas(fnSeg, res, background=background)
```